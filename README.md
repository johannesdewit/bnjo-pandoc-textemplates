# Pandoc BNJO TeX Template
A repository containing templates for converting [Brill Plain Text (BPT)](https://brillpublishers.gitlab.io/documentation-brill-plain-text/) files into formats suitable for proofreading, typesetting and digital publishing.

## Installation
1. Install Pandoc from [https://pandoc.org/](https://pandoc.org).
You will also need [Luatex](https://luatex.org/) for this specific template to work.
2. Download the templates (.latex files) from this repository.
3. Place the .latex file of the template you want in the Pandoc templates folder
(If absent, create the folder):
  - Unix, Linux, macOS: `/Users/USERNAME/.local/share/pandoc/templates/` or `/Users/USERNAME/.pandoc/templates/`
  - Windows Vista or later: `C:\Users\USERNAME\AppData\Roaming\pandoc\templates\`
4. You can now call these templates when using Pandoc via the commandline by using the `--template templatename.latex` flag. Make sure to also use `--pdf-engine="lualatex"` for these templates to work.

... To be expanded.

The next steps apply when using [Obisidian](https://obsidian.md/) and the
[Obsidian Pandoc Plugin](https://github.com/OliverBalfour/obsidian-pandoc).

... To be expanded.

## Goal and background
The goal of this project is to develop several different custom [Pandoc templates](https://pandoc.org/MANUAL.html#templates) that will provide a way to convert the BPT files to a formats needed for specific applications. For now the following templates are being considered:

- [ ] A template for generating PDFs in the BNJO IV style as a proof of concept.
- [ ] A template for generating PDFs in the BTS style, which might be offered
as possible downloads in the reader.
- [ ] A template for generating DOCX files optimal for communicating needed edits.

This project is part of a bigger effort to improve the BNJO workflow. By providing a way in which the authors and copy editors can convert their BPT-files locally they can validate the outcome of their edits. It also provides greater flexibility in picking a format that lends itself best for communicating about edits and problems. We are going with Pandoc because it provides a way to convert markdown into a plethora of different formats already. It is also available as a plug-in in several markdown editors (e.g. Obsidian and Typora), but it can also be executed as a commandline tool. On top of that this is also used in the present DOCX > BPT > DOCX pipeline.

The [BNJO Luafilters](https://gitlab.com/johannesdewit/bnjo-pandoc-luafilters) are used in order to modify the order of the file and are thus also needed for a complete conversion.

## Status and process
The process of designing such templates has, as far as I know not been documented and I have not been able to find a good tutorial on how to proceed. While [Pandoc's own documentation](https://pandoc.org/MANUAL.html#templates) does contain an entry on the templates, there are no further directions on how to approach building these templates.

### Approach 1 - Creating a metadata-file (Left because Approach 2 was more attractive for ease of use.)
Pandoc allows for metadata files which define variables to be interpreted by Pandoc (see [Pandoc documentation](https://pandoc.org/MANUAL.html#templates)). It is a possibility to use this to create a standard `settings.yml` file which provides Pandoc with the right information (the right variables) in order to apply the right fonts etc.

#### Step 1: The starting point
As a starting point we look at the output of the conversion using default Pandoc. A couple things occur as being not right immediately.
- The entry title page must be seperate and styled.
  - It should not portray the date
- Margins are incorrect.
- Greek is not displayed.
- Font type is not right
- Font size is not right
- Bibliography citeproc conversion is not executed.

#### Step 2: First adjustments
- [ ] Create a `bnjo_pandoc_texsettings.yml` file
- [ ] Define settings in `bnjo_pandoc_texsettings.yml`
  - [ ] Define font type and size
  - [ ] Define margins
  - [ ] Define documenttype.

### Approach 2 - Using a header-file
Pandoc allows for header files to be specified which are used in order to set document-wide specifications using the `-H header_file.tex` flag.

#### Step 1: Using the `bpt-converter` header file.
In this attempt I copied the header file that was made for use in the [bpt-converter](https://gitlab.com/brillpublishers/code/bpt-converter) and tried this to construct a pdf. This header file already specifies a lot of things for the pdf output, like fonts and margins.

Outcome: This allows for font and margin definition without a seperate settings file.

#### Step 2: Problems that occur.
- Headings are numbered.
- Headings are not styled appropriately.
- Bibliography is not citeproc processed.
- Metadata codeblocks are still shown.

### Approach 3 - Writing a template-file from scratch.
Another possibility is to construct a new template file. This is like the [`eisvogel.tex`](https://github.com/Wandmalfarbe/pandoc-latex-template) template. I like this approach because it only needs one extra flag to be defined.

#### Step 1: Construct a basic template
I will use the template made for the bpt-converter as a start. This will provide a framework which can be built upon.

We can use the standard [Pandoc defined variables](https://pandoc.org/MANUAL.html#variables-set-automatically) to render a output.

#### Step 2: Required preamble
Added required lines of code without which a pdf could not be generated because of the pandoc-specific latex commands.

### Broader problems:
- There still seems to be a problem for interpreting parallel texts in Pandoc. Because Reledpar (which is used to typeset parallel texts) requires two tex environments, both with independent p starts, ledgroups, etc. The single Pandoc `$body$` variable is not capable enough since it can only be tempered with as a whole.

In order to deal with this problem one could customize the reader to spit out multiple variables. One 'editions' variable, a 'translations' variable, a 'work_commentary', a 'bibliography' and a 'commentaries' variable. I think this is the route to take when we do not want the user of the conversion to input more information within the bpt-file itself.

We are however modifying Pandoc to the extent that it does not seem logical anymore, because we have the bpt-converter doing basically the same things.

If we were to give up on the parallel text however, and could live with just every top down, there is still a possibility to use pandoc. But this is __NOT__ viable for typesetting, just for editing of the content.  

### Approach 4 - Adjusting the default template file.
Because of the problems that came up when trying approach number 3, I will first attempt to create a goodlooking LaTeX template that adheres as close a possible to the BTS (Brill Typographic Styleguide).

The first test is a conversion from standard markdown to pdf via TeX: `bpt_template.latex`. I will not test this on the bpt files yet, since these have their own problems (see approach 3).

## Unsolved problems
- Indented paragraph after blockquote. This should not happen.
- Footnotemarkers are superscript, this should be old style tabular, normal size.
- Footnotemarkers in single digits should be followed by a 4mm margin for the footnote text, this is not the case. It follows 8mm all the time, disregarding the dependency on the number of digits of the footnotemarker.
- The footnote section does not 'stick' to the bottom of the body.
- Numbered lists cannot be assigned alphabetic numbers nor roman numerals.
- Enumerations and lists cannot be specified in markdown as being part of a paragraph. Which means the possible blank line specified on page 5 of the BTS cannot be implemented when converting from markdown to pdf via tex.
- Around blockquotes should be a 4.75mm blank line, this is not the case yet.
- New paragraphs in footnotes should be indented. This is not yet the case.
- New paragraphs after lists should not be indented.
- Headings provide a challenge.
  - Because markdown only allows for 6 heading levels, while the BTS defines up to 8 levels.
  - In markdown, the six levels need also include the chapter and section markers. In pandoc it is however possible to define which level in latex corresponds to which level in markdown, I believe. I have chosen to use lvl 1 and lvl 2 as section headers and chapter headers resp. The title can already be defined in the metadata on the top of the markdown file.
  - A way needs to be found to translate markdown headings levels 1 and 2 to chapters and parts in latex.
  - More levels could be defined in the latex, which might not be used when converting from markdown however.
  - The BTS also supposes that the headings are always numbered, but is this number also given in the bpt than this will result in strange looking headings.
